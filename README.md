# PostgreSQLBench

An experimental benchmarking tool for a PostGreSQL database

## Requirements
* Currently requires PostGreSQL to be installed and configured with a user/password

## Install PostGreSQL
### MacOS
```bash
brew install postgresql
brew services start postgresql
```

### Linux
```bash
COMING SOON
```

## Usage
```aidl
./postgreSQLBench-MacOS
```

## Example output
```
pgbench (14.2)
starting vacuum...end
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 50
query mode: simple
number of clients: 10
number of threads: 2
number of transactions per client: 10000
number of transactions actually processed: 100000/100000
latency average = 0.953 ms
initial connection time = 38.303 ms
tps = 10489.335912 (without initial connection time)
```

## CODEBASE UNDER CONSTRUCTION
