BINARY_NAME=postgreSQLBench

.DEFAULT_GOAL := build

fmt:
		go fmt ./...
.PHONY:fmt

lint: fmt
		golint ./...
.PHONY:lint

vet:fmt
		go vet ./...
.PHONY:vet

run: build
		go run -race postgreSQLBench.go
.PHONY: run

build: vet
		GOARCH=amd64 GOOS=darwin go build -o ${BINARY_NAME}-MacOS postgreSQLBench.go
.PHONY:build