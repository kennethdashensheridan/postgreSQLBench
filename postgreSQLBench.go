package main

import (
	"fmt"
	"os"
	"os/exec"
)

var PgbenchMacOS string = "/usr/local/bin/pgbench"
var postgresqlMacOS string = "/usr/local/var/postgres"

func main() {
	// set location of MacOS binary
	// set location of Linux binary
	// var postgresqlLinux = "" // EXPERIMENTAL not a completed feature

	fmt.Println("Checking if PostgreSQL is installed")

	// Check if postgres is installed
	checkFileExists(postgresqlMacOS) // program will exit upon failure
	//Check if pgbench is installed
	checkFileExists(PgbenchMacOS)
	fmt.Println("Continuing on with test")

	createTestDB()
	establishBaseline()
}

//*****************************************************************
//*****************************************************************
// fileExists checks if a file exists and is not a directory before
// we try using it to prevent further errors.
func checkFileExists(filename string) {
	if _, err := os.Stat(filename); err == nil {
		fmt.Printf("%v file found -- PASS\n", filename)
	} else {
		fmt.Printf("File %v not found, exiting -- FAIL\n", filename)
		os.Exit(3)
	}
	return
}

func createTestDB() {
	fmt.Println("Creating test database")
	// Thi step will create 4 tables - which host the test date rows and other metadata.
	out, err := exec.Command(PgbenchMacOS, "-U", "postgres", "-p", "5432", "-i", "-s", "50", "mydb").CombinedOutput()
	// "-i": initialize the database. Creates a bunch of default tables
	// "-s": scaling option. Take the default rows and x 50 or whatever the scaling number you want.
	if err != nil {
		// Panic and Exit
		panic(err.Error())
	}
	// Use "out"
	createTestDB := string(out[:])
	fmt.Printf(createTestDB)

}

func establishBaseline() {
	fmt.Println("Establishing a baseline.")
	baselineOut, err := exec.Command(PgbenchMacOS, "-U", "postgres", "-p", "5432", "-c", "10,", "-j", "2", "-t", "10000", "mydb").CombinedOutput()
	// "-c": number of clients
	// "-j 2": number of threads
	// "-t": amount of transactions, these values are 10000 transactions per client. So 10 x 10000 = 100,000 transactions.
	if err != nil {
		// Panic and Exit
		panic(err.Error())
	}
	establishBaseline := string(baselineOut[:])
	fmt.Printf(establishBaseline)
}
